---
sidebar_position: 1
---

# Tutoriel Intro

Découvrez **Le CMS Redbox en moins de 15 minutes**.
# CMS 4.0


**Le CMS 4.0** est une solution basée sur le [Framework PHP Symfony](https://github.com/symfony/symfony) permettant l'élaboration rapide d'une **structure de site internet**.

Ce Projet est propulsé par l'équipe

![Redbox Communication logo](/img/logo-redbox.svg "Redbox Communication logo")

