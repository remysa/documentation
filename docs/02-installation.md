---
sidebar_position: 2
---
# Installation du projet

## Prérequis
*** Partie à compléter ***

## Clonage du projet
Clonez le projet dans votre répertoire local.

*** Partie à compléter ***

## Configuration du projet

Une fois le projet cloné, crée un fichier *.env.local* à la racine du projet.

```bash
    # In all environments, the following files are loaded if they exist,
    # the latter taking precedence over the former:
    #
    #  * .env                contains default values for the environment variables needed by the app
    #  * .env.local          uncommitted file with local overrides
    #  * .env.$APP_ENV       committed environment-specific defaults
    #  * .env.$APP_ENV.local uncommitted environment-specific overrides
    #
    # Real environment variables win over .env files.
    #
    # DO NOT DEFINE PRODUCTION SECRETS IN THIS FILE NOR IN ANY OTHER COMMITTED FILES.
    #
    # Run "composer dump-env prod" to compile .env files for production use (requires symfony/flex >=1.2).
    # https://symfony.com/doc/current/best_practices.html#use-environment-variables-for-infrastructure-configuration
    ###> symfony/framework-bundle ###
    APP_ENV=dev
    APP_SECRET=bf09f02526f9c490f1537c20607caf79
    ###< symfony/framework-bundle ###
    ###> symfony/mailer ###
    # MAILER_DSN=smtp://localhost
    ###< symfony/mailer ###
    ###> doctrine/doctrine-bundle ###
    # Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
    # IMPORTANT: You MUST configure your server version, either here or in config/packages/doctrine.yaml
    #
    # DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
    DATABASE_URL="mysql://username:password@127.0.0.1:3306/database_name?serverVersion=mariadb-10.3.22"
    
    ASSET_URL="/build"
    ###< doctrine/doctrine-bundle ###
```
Mettre à jour le fichier *.env.local* avec vos configurations locales.

Une fois le fichier créé, exécutez à la racine du projet les commandes suivantes :
```bash
composer install
```

Vous pouvez lancer le serveur avec la commande suivante :
```bash
symfony server:start
```

### Installation des assets

Pour installer les assets en environnement de **dev**. Assurez-vous d'avoir au la version **14** de Node et la version **6** de NPM puis installer les packages en exécutant la commande :

```bash
npm install

#En cas de problème d'installation, utiliser:
npm install --force

#En cas de problème de droit, utiliser:
sudo npm install
```

Si vous souhaitez utiliser le serveur interne de symfony, ajouter dans votre fichier *env.local* :
```bash
ASSET_URL="/build"
```

Si vous passez par votre serveur Apache:
```bash
ASSET_URL="http://localhost/keolis-crm/public/build"
```
Aucune configuration supplémentaire n'est nécessaire pour ce qui est de l'environnement de **prod**

Une fois les packages installés, il est nécessaire de construire les fichiers js et css en exécutant la commande :
```bash
#Si vous souhaitez construire les assets une seule fois
npm run dev

#Si vous souhaitez que les assets soient reconstruit à chaque changement
npm run watch
```

### Import des données

#### Mise à jour de la base de données

Si vous installez ce projet pour la première fois vous ne possédez peut être pas d'une base de donné, pour la créer, exécuter cette commande:
```bash
bin/console doctrine:database:create
```

Pour mettre à jour le schéma de votre base de données, exécuter la commande:
```bash
bin/console doctrine:migrations:migrate
```

Afin de peupler votre base de données avec un minimum de contenu nous avons développé une commande d'import de données.

```bash
# dev / prod - import des données utiles au fonctionnement du site sans nettoyage de la base de données
php bin/console cms:database:update --force
```

## Ajout de multi-langues

Le CMS utilise Knp Translatable afin de gérer le multilangue.

Afin d'ajouter ou retirer des langues cela se fait via 2 fichiers:

***coonfig/services.yaml***
```yaml
parameters:
    // Liste des langues sous forme de tableau
    app_locales: [fr, en, es]
    
    // Liste des langues sous forme de string
    app_locales_pipe: 'fr|en|es'
```

***config/routes/annotations***
```yaml
controllers:
    // Prefix ajouté sur toutes les routes du site
    prefix:
        fr: ''          // Pas de préfix pour la langue par défaut
        en: '/en'
        es: '/es'
```

Les formulaires en multi-langue sont gérés via le bundle de A2lix, qui s'intègre au Translatable de Knp :

[https://a2lix.fr/bundles/translation-form/3.x.html](https://a2lix.fr/bundles/translation-form/3.x.html)

## Services

## Génération de statistiques

Le CRM dispose d'un système de génération automatique de graph de statistiques. Le principe est de créer un élément HTML avec un attribut **id** identique à l'une des clés du tableau de statistiques retournées par le *StatsController*. De cette façon, la partie JS est capable de générer le graph demandé. Le but de cette fonctionnalité est donc de pouvoir générer des graphs de statistique sans avoir à écrire de JS.

#### Initialisation

Sur une page où des statistiques doivent être intégrées :
```javascript
    document.addEventListener("DOMContentLoaded", function () {
        let stats = new Stats();
        stats.update()
    });
```
La méthode *update* retourne une promesse, il est donc possible d'enchainer des actions supplémentaires avec un *then*.

#### Requête

Par défaut, les données sont récupérées via un *fetch* sur l'URL **"url_du_site/user/stats"**, mais ce paramètre peut être modifié lors de l'instanciation de la classe *Stat* ou lors de l'update.

Pour des raisons de simplicité, toutes les statistiques sont retournées en même temps mais selon la quantité de données à traiter cela peut être plus ou moins long. De plus, les statistiques à afficher ne sont pas forcément les mêmes selon les pages. Le paramètre **ids** permet de lister les statistiques à prendre, les autres seront ignorées par le controller.

Il est également possible d'ajouter des paramètres au *fetch*, par exemple pour faire passer des dates de début et de fin ou l'id d'un utilisateur.

```javascript
//Sous forme de chaine de caractère
let stats = new Stats({
    ids: "&stat_filter[]=test_graph_card&stat_filter[]=test_graph_bar",
    urlParameters: "&date_start=2021-04-01&date_end=2021-04-30"
});

//Sous forme de tableau/objet
let stats = new Stats({
    ids: ["test_graph_card", "test_graph_bar"],
    urlParameters: {
        "date_start": "2021-04-01",
        "date_end": "2021-04-30"
    }
});

//Sous forme de callback
let stats = new Stats({
    ids: function () {
        return ["test_graph_card", "test_graph_bar"];
    },
    urlParameters: function () {
        return "&date_start=2021-04-01&date_end=2021-04-30";
    }
});

/*
 * Exemple où tous les graphs sont récupérés une première 
 * fois puis seulement deux des graphs s'actualisent 
 * toutes les 5 secondes
 */
let stats = new Stats();
stats.update().then(() => {
    setInterval(function () {
        stats.update({
            ids: function () {
                return ["test_graph_card", "test_graph_bar"];
            }
        })
    }, 5000);
});
```

#### Format de données
Les données retournées par le controller en JSON doivent être correctement formatée pour chaque type de graph afin d'être correctement utilisée.

- Type **'card'**:

Ce type permet l'insertion d'un simple chiffre, text ou d'un élément HTML.
```php
$graphs["id_graph_card"] = [
    'type' => 'card',
    'data' => '<span class="badge bg-success">50</span>',
];
```
- Type **'bar'**:

Ce type permet la création de graphs batons.
```php
$graphs["id_graph_bar"] = [
    'type' => 'bar',
    'categories' => ['2021-02', '2021-03', '2021-04'],
    'data' => [
        [
            "name" => 'Validé',
            'data' => [2, 69, 59]
        ],
        [
            "name" => 'En attente',
            'data' => [52, 41, 10]
        ]
    ],
    'tooltip' => '%val% %'
];
```
**tooltip** permet de formatter les tooltips qui s'affichent lors du hover sur le graph, le **%val%** est remplacé par la valeur de la colonne.

- Type **'pie'**/**'donut'**:

Ces deux types de graphs étant identiques en terme de format de données, les deux exemples sont regroupés ici.
```php
$graphs["id_graph_pie"] = [
    'type' => 'pie', //ou 'donut'
    'labels' => ['2021-02', '2021-03'],
    'data' => [52, 69],
    'tooltip' => '%val% $'
];
```

Afin de prendre en compte, la présence de filtre qui permettent de récupérer uniquement certaines données, il est nécessaire de récupérer la liste des filtre et d'encadrer toutes vos générations de stats avec la condition suivante:
```php
$filter = [];
$all = false;

//Les filtres sont obligatoirement passés avec la paramètre "stat_filter"
if ($request->get('stat_filter') !== null and is_array($request->get('stat_filter'))) {
    //Si des filtres sont présents on les récupére
    $filter = $request->get('stat_filter');
} else {
    //Sinon on retourne toutes les stats
    $all = true;
}

if($all == true or in_array('id_graph_pie', $filter)) {
    //Ajout des votre stat au tableau qui sera retourné par le controller
    
    // Ici si "id_graph_pie" n'est pas présent dans la liste des
    // filtres, ce graph ne sera pas affiché ou mis à jour.
}
```

#### Loader

Les requêtes pouvant être un peu longue selon la quantité de données à récupérer, il est possible de paramètrer l'affichage d'un loader qui apparait avant que la requête soit effectué et disparait une fois les données récupérées et traitées. Pour cela, il suffit de renseigner l'id de l’élément dans lequel le loader doit être inséré via le paramètre **loaderElement**.

Par défaut un *Spinner* bootstrap est utilisé comme loader mais celui-ci peut être changé via le paramètre *loaderHTML*.

```javascript
let stats = new Stats({
    loaderElement: '#stat-loader',
    loaderHTML: '<div class="spinner-border spinner-border-sm text-primary" role="status"><span class="visually-hidden">Loading...</span></div>' //Spinner par défaut
});
```

## Génération automatique d'export CSV

Il est possible de générer des exports CSV simple automatiquement en utilisant le service **CsvExport**.

#### Utilisation
```php
$clients = $clientRepository->findAll();

return $export->generateCsvExportResponse('export_client', $clients, [
        'lastname', 'firstname', 'city'
    ], [
        'Nom', 'Prénom', 'Ville'
    ]);
```

La méthode **generateCsvExportResponse** accepte 5 paramètres:
- 'filename', *string* : nom du fichier CSV téléchargé, l'extension '.csv' n'est pas obligatoire car ajoutée automatiquement si elle n'est pas présente.
- 'datas', *array* : liste des données à exporter. Dans l'exemple ci-dessus il s'agit d'une liste d'entité Client.
- 'methods', *array* : liste des méthodes à utiliser. Chaque méthode correspond à une colonne de votre export. Dans l'exemple ci-dessus, la première colonne contient les noms des clients, la méthode à utiliser est donc **'lastname'**. Le service va alors aller chercher la donnée en appelant la méthode *lastname()* ou *getLastname()* de l'entité Client. Idem pour **'firstname'** et **'city'**
- 'header', **'array'** : Tableau contenant la liste des noms de colonnes à mettre dans le CSV. Peut être laissé vide pour ne pas mettre de header.
- 'footer', **'array'** : Tableau contennant la liste des champs à placer tout en bas du CSV. Peut être laissé vide pour ne pas mettre de footer.

#### Limite

Ce service dépends de la capacité à fournir des données via de simples *getter* dans une entité. Il est possible de créer des *getter* personnalisés, par exemple pour retourner l'addition de plusieurs champs ou un texte formaté. Mais cette méthode a également ses limites, notamment lorsque l'utilisation d'un service est nécessaire pour réaliser un calcul plus complexe ou lorsque qu'une requête SQL doit être effectuée.

Ce service est donc limité à des exports simple qui conviendront dans beaucoup de cas mais ne fonctionnera pas sur des exports plus complexes.

## Table Configuration

La table **config** a beaucoup changé par rapport à la version précédente du CMS. Notamment, il est maintenant possible de catégoriser les **config** grâce à l'entité *ConfigCategory*. Voici les nouveaux attribut important de l'entité *Config*:
- isShown: **bool** - Permet de contrôler l'affichage de la config sur d'autres rôles utilisateurs. Cet attribut n'est pas pris en compte sur un utilisateur Redbox qui par défaut peut voir tous les configs.
- isSecured: **bool** - Permet à tous les utilisateurs de voir que cette configuration existe mais sans pouvoir voir sa valeur ou la modifier. Cet attribut n'est pas pris en compte sur un utilisateur Redbox qui par défaut peut voir et modifier tous les configs.
- parameters: **json** - Cet attribut est réglé par Redbox, il permet notamment de donner une liste de paramètre qui pourront être sélectionnés dans un *ChoiceType* par exemple.

#### Types pris en charge

La nouvelle table **config** prends en charge plus de type de variable: Les types de donnée pris en charge sont:

|       Type        |   FormType   | Var Type  | Description                                                                                                                      |
|:-----------------:|:------------:|:---------:|----------------------------------------------------------------------------------------------------------------------------------|
|    **string**     |   TextType   |  string   | Texte simple sans HTML                                                                                                           |
|     **email**     |  EmailType   |  string   | Génère un input HTML5 de type email qui sera donc bloqué par le navigateur si l'email est mal formaté                            |
|     **phone**     |   TelType    |  string   | Génère un input HTML5 de type tel                                                                                                |
|      **url**      |   UrlType    |  string   | Génère un input HTML5 de type url                                                                                                |
|   **textarea**    | TextareaType |  string   | Génère un textarea qui peut contenir du HTML, un "**&#124;raw**" est doit être appliqué en twig pour les configs de ce type      |
| **single_choice** |  ChoiceType  |  string   | Génère un select permettant de choisir une valeur parmi celles présentent dans l'attribut *parameters*                           |
| **multi_choice**  |  ChoiceType  |   array   | Génère un select permettant de choisir plusieurs valeurs parmi celles présentent dans l'attribut *parameters*                    |
|    **integer**    | IntegerType  |  integer  | Permet de saisir un nombre entier. il est possible via l'attribut *parameters* de régler des valeurs de *min*, *max* et *step*   |
|     **float**     |  NumberType  |   float   | Permet de saisir un nombre flottant. il est possible via l'attribut *parameters* de régler des valeurs de *min*, *max* et *step* |
|    **boolean**    | CheckboxType |   bool    | Génère une case à cocher                                                                                                         |
|   **datetime**    | DateTimeType | \DateTime | Génère un champs avec un datepicker                                                                                              |
|     **file**      |   FileType   |  string   | Génère un input file permettant l'upload d'un document                                                                           |
|     **array**     |   TextType   |   array   | Permet la saisi d'un tableau sous forme de texte. Chaque valeur doit être séparé par un "**;**"                                  |

#### Transformation des données

Toutes les valeurs de la table config sont stockées dans l'attribut "**value**" sous forme de texte et converties automatiquement lors de leur récupération au format spécifié dans la colonne "*Var Type*".

#### Utilisation

- PHP, Injection de dépendance:

```php
public function __construct(ConfigHandler $config) {
    $value = $config->getConfig("test_config");
}
```

- Twig 

La totalité des configurations est accessible via la variable globale "**config**" en Twig:  
```twig
{{ config.test_config }}
```

## Utilisation de dropzone.js
Pour utiliser le service dropzone.js pour l'import et la liaison des documents sur une entité, il suffit d'ajout un field dans le *formType* de l'entité.
```php
->add('uploads', EntityType::class, [
    'class' => Upload::class,
    'choice_label' => 'id',
    'row_attr' => ['class' => 'd-none'],
    'attr' => ['class' => 'form-control uploads-files'],
    'multiple' => true,
    'expanded' => false,
    'required' => false,
    'label' => 'Uploads',
    'mapped' => true,
    'by_reference' => false,
]);
```
Il suffit par la suite d'ajouter dans la vue *_form.html.twig* de l'entité
```html
    <div id="dropzone" 
        class="dropzone mb-3" 
        action="{{ path('dropzone_upload_secured')}}" 
        data-classname="uploads-files" 
        data-maxfilesize="1" 
        data-acceptedfiles=".png, .jpg, image/png, image/jpg">
    </div>
```
La **div** a besoin de plusieurs attributs :
- **action** : route utilisée pour enregistrer l'image en base de donnée (dropzone_upload_secured ou dropzone_upload_public) 
- **data-classname** : Nom de la classe utilisée dans le formType dans l'attribut "attr => 'class'"
- **data-maxfilesize** : Taille maximale autorisée
- **data-acceptedfiles** : Liste des extensions de fichier autorisées

*Il est possible d'utiliser plusieurs fois le dropzone sur une même entité en renseignant des noms de classes différents*

